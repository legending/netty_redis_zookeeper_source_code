package com.crazymakercircle.netty.testCases;

        import com.crazymakercircle.util.Logger;
        import io.netty.buffer.ByteBuf;
        import io.netty.buffer.ByteBufAllocator;
        import org.junit.Test;

public class PooledByteBufTest {

    @Test
    public void poolTest() {
        Logger.info("测试buf回收====");
        ByteBufAllocator allocator = ByteBufAllocator.DEFAULT;
        // tiny
        ByteBuf buf1 = allocator.directBuffer(495); // 分配的内存最大长度为496
        Logger.info("buf1: 0x%X%n", buf1.memoryAddress());
        buf1.release(); // 此时会被回收到tiny 的512b格子中
    }
}